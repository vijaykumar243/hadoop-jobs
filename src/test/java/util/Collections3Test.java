package util;

import org.junit.Test;

import java.util.List;

import static org.apache.hadoop.thirdparty.guava.common.collect.Lists.newArrayList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static util.Collections3.*;

public class Collections3Test {

    List<Integer> input = newArrayList(1, 3, 5, 2, 10, 4);

    @Test
    public void foldLeftSum() {
        Integer result = foldLeft(input, 0, new Function2<Integer, Integer>() {
            @Override
            public Integer apply(Integer total, Integer value) {
                return total + value;
            }
        });
        assertThat(result, is(25));
    }

    @Test
    public void foldLeftMax() {
        Integer result = foldLeft(input, Integer.MIN_VALUE, new Function2<Integer, Integer>() {
            @Override
            public Integer apply(Integer i1, Integer i2) {
                return Math.max(i1, i2);
            }
        });
        assertThat(result, is(10));
    }

    @Test
    public void foldLeftConcat() {
        String result = foldLeft(input, "", new Function2<Integer, String>() {
            @Override
            public String apply(String s, Integer integer) {
                return s + integer.toString();
            }
        });
        assertThat(result, is("1352104"));
    }

    @Test
    public void applyMap() {
        List<String> result = map(input, new Function1<Integer, String>() {
            @Override
            public String apply(Integer integer) {
                return integer.toString();
            }
        });
        assertThat(result, equalTo((List<String>)newArrayList("1", "3", "5", "2", "10", "4")));
    }

    @Test
    public void applyFilter() {
        List<Integer> result = filter(input, new Function1<java.lang.Integer, java.lang.Boolean>() {
            @Override
            public Boolean apply(Integer integer) {
                return integer > 5;
            }
        });
        assertThat(result, equalTo((List<Integer>)newArrayList(10)));
    }
}
