package hadoopbook.ch5;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.apache.hadoop.thirdparty.guava.common.collect.Lists.newArrayList;

public class MaxTemperatureMapperReducerTest {

    MapDriver mapDriver = new MapDriver<LongWritable, Text, Text, IntWritable>();

    @Before
    public void setupMapper() {
        MaxTemperatureMapperReducer mapperReducer = new MaxTemperatureMapperReducer();
        mapDriver.setMapper(mapperReducer.createMapper());
    }

    @Test
    public void negativeTemperature() {
        String input = "0067011990999991950051507004+68750+023550FM-12+038299999V0203301N00671220001CN9999999N9-00111+99999999999";
        testWith(input, -11);
    }

    private void testWith(String input, Integer expectedTemperature) {
        mapDriver.withInput(new LongWritable(), new Text(input));
        if(expectedTemperature != null) {
            mapDriver.withOutput(new Text("1950"), new IntWritable(expectedTemperature));
        }
        mapDriver.runTest();
    }

    @Test
    public void positiveTemperature() {
        String input = "0067011990999991950051507004+68750+023550FM-12+038299999V0203301N00671220001CN9999999N9+00211+99999999999";
        testWith(input, 21);
    }

    @Test
    public void ignoreMissingTemperature() {
        String input = "0067011990999991950051507004+68750+023550FM-12+038299999V0203301N00671220001CN9999999N9+99991+99999999999";
        testWith(input, null);
    }

    ReduceDriver reduceDriver = new ReduceDriver<Text, IntWritable, Text, IntWritable>();

    @Before
    public void setupReduceDriver() {
        MaxTemperatureMapperReducer mapperReducer = new MaxTemperatureMapperReducer();
        reduceDriver.setReducer(mapperReducer.createReducer());
    }

    @Test
    public void returnsMaximumIntegerInValues() {
        List<IntWritable> values = newArrayList(new IntWritable(10), new IntWritable(5), new IntWritable(7));
        reduceDriver.withInput(new Text("1950"), values);
        reduceDriver.withOutput(new Text("1950"), new IntWritable(10));
        reduceDriver.runTest();
    }

}
