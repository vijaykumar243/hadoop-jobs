package hadoopbook.ch5;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.apache.hadoop.thirdparty.guava.common.collect.Lists.newArrayList;

public class MaxTemperatureReducerTest {

    ReduceDriver reduceDriver = new ReduceDriver<Text, IntWritable, Text, IntWritable>();

    @Before
    public void setupReduceDriver() {
        reduceDriver.setReducer(new MaxTemperatureReducerV2());
    }

    @Test
    public void returnsMaximumIntegerInValues() {
        List<IntWritable> values = newArrayList(new IntWritable(10), new IntWritable(5), new IntWritable(7));
        reduceDriver.withInput(new Text("1950"), values);
        reduceDriver.withOutput(new Text("1950"), new IntWritable(10));
        reduceDriver.runTest();
    }

}
