package hadoopbook.ch5;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.util.ToolRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class MaxTemperatureDriverTest {

    MapReduceDriver mapReduceDriver = new MapReduceDriver<LongWritable, Text, Text, IntWritable, Text, IntWritable>();
    public static final String INPUT = "src/main/resources/sample.txt";
    public static final String OUTPUT = "target/output";

    @Before
    public void setupMapReducer() {
        mapReduceDriver.setMapper(new MaxTemperatureMapperV2());
        mapReduceDriver.setReducer(new MaxTemperatureReducerV2());
    }

    @Before
    @After
    public void deleteOutputDir() throws IOException {
        FileUtils.deleteDirectory(new File(OUTPUT));
    }

    @Test
    public void runMapperAndReducer() throws IOException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("sample.txt");
        List<String> lines = IOUtils.readLines(inputStream);
        for(String line : lines) {
            mapReduceDriver.withInput(null, new Text(line));
        }
        mapReduceDriver.withOutput(new Text("1949"), new IntWritable(111));
        mapReduceDriver.withOutput(new Text("1950"), new IntWritable(22));
        mapReduceDriver.runTest();
    }

    @Test
    public void runDriver() throws Exception {
        int result = ToolRunner.run(new MaxTemperatureDriverV2(), new String[]{INPUT, OUTPUT});
        assertThat(result, is(0));
        assertThat("_SUCCESS exists", new File(OUTPUT +"/_SUCCESS").exists(), is(true));
        File part = new File(OUTPUT + "/part-r-00000");
        assertThat("Part file exists", part.exists(), is(true));
        List<String> lines = IOUtils.readLines(new FileReader(part));
        assertThat("2 results", lines.size(), is(2));
        assertThat(lines.get(0), equalTo("1949\t111"));
        assertThat(lines.get(1), equalTo("1950\t22"));
    }
}
