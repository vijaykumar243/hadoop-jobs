package hadoopbook.ch5;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.util.Tool;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

abstract public class LocalDriverTest {

    Configuration conf;
    Path input;
    Path output;

    @Before
    public void setupEnv() throws IOException {
        conf = new Configuration();
        conf.set("fs.default.name", "file:///");
        conf.set("mapred.job.tracker", "local");

        input = new Path(getInputPath());
        output = new Path("target/output");

        deleteOutputPath(conf, output);
    }

    private void deleteOutputPath(Configuration conf, Path output) throws IOException {
        FileSystem fs = FileSystem.getLocal(conf);
        fs.delete(output, true);
    }

    abstract String getInputPath();

    @Test
    public void runDriver() throws Exception {
        Tool driver = createDriver();
        driver.setConf(conf);

        int result = driver.run(new String[]{input.toString(), output.toString()});
        assertThat(result, is(0));

        verifyOutput();
    }

    protected abstract void verifyOutput() throws IOException;

    protected abstract Tool createDriver();
}
