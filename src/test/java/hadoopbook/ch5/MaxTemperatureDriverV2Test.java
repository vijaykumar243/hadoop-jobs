package hadoopbook.ch5;

import org.apache.commons.io.IOUtils;
import org.apache.hadoop.util.Tool;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class MaxTemperatureDriverV2Test extends LocalDriverTest {

    @Override
    String getInputPath() {
        return "src/main/resources/sample.txt";
    }

    @Override
    protected Tool createDriver() {
        return new MaxTemperatureDriverV2();
    }

    @Override
    protected void verifyOutput() throws IOException {
        String part = output.toString() + "/part-r-00000";
        List<String> lines = IOUtils.readLines(new FileReader(part));

        assertThat("2 results", lines.size(), is(2));
        assertThat(lines.get(0), equalTo("1949\t111"));
        assertThat(lines.get(1), equalTo("1950\t22"));
    }
}
