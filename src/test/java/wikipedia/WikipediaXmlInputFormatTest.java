package wikipedia;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordReader;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class WikipediaXmlInputFormatTest extends WikipediaXmlRecorderReaderTestBase {

    @Test
    public void shouldReturnWikipediaXmlRecordReader() throws Exception {
        setupRecordReaderParams(0, 1000, TITLE_TAG);
        WikipediaXmlInputFormat inputFormat = new WikipediaXmlInputFormat();
        RecordReader<Text,Text> recordReader = inputFormat.createRecordReader(fileSplit, context);
        assertThat(recordReader instanceof WikipediaXmlRecorderReader2, is(true));
    }
}
