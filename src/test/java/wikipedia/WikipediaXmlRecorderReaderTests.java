package wikipedia;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordReader;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.internal.matchers.TypeSafeMatcher;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

abstract public class WikipediaXmlRecorderReaderTests extends WikipediaXmlRecorderReaderTestBase {

    public static final String NO_TAG = null;
    private RecordReader<Text, Text> recordReader;

    public abstract RecordReader<Text, Text> createRecordReader();

    private void setupRecordReaderWith(long start, long length, String tag) throws Exception {
        setupRecordReaderParams(start, length, tag);

        this.recordReader = new WikipediaXmlRecorderReader2();
        this.recordReader.initialize(fileSplit, context);
    }

    @Test
    public void readerShouldReadAllPageParts() throws Exception {
        setupRecordReaderWith(0, getFullFileLength(), NO_TAG);
        List<String> pages = readAllParts();
        assertThat(pages.size(), is(37));
    }

    @Test
    public void readerShouldDefaultReadPageTagParts() throws Exception {
        setupRecordReaderWith(0, getFullFileLength(), NO_TAG);

        assertThat(this.recordReader.nextKeyValue(), is(true));
        assertThat(this.recordReader.getCurrentKey(), equalTo(new Text("page")));
        assertThat(this.recordReader.getCurrentValue().toString(), startsWith("<page>"));
        assertThat(this.recordReader.getCurrentValue().toString(), endsWith("</page>"));
    }

    private Matcher<String> startsWith(final String prefix) {
        return new TypeSafeMatcher<String>() {
            @Override
            public boolean matchesSafely(String text) {
                return text.startsWith(prefix);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("startsWith").appendValue(prefix);
            }
        };
    }

    private Matcher<String> endsWith(final String suffix) {
        return new TypeSafeMatcher<String>() {
            @Override
            public boolean matchesSafely(String text) {
                return text.endsWith(suffix);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("endsWith").appendValue(suffix);
            }
        };
    }

    @Test
    public void readRecord() throws Exception {
        setupRecordReaderWith(0, getFullFileLength(), TITLE_TAG);
        assertThat(this.recordReader.nextKeyValue(), is(true));
        assertThat(this.recordReader.getCurrentKey(), equalTo(new Text("title")));
        assertThat(this.recordReader.getCurrentValue(), equalTo(new Text("<title>Albert Speer</title>")));
    }

    private long getFullFileLength() {
        return new File(XML_FILE).length();
    }

    @Test
    public void readAllRecords() throws Exception {
        setupRecordReaderWith(0, getFullFileLength(), TITLE_TAG);
        List<String> parts = readAllParts();

        assertThat(parts.size(), is(37));
        assertThat("List contains all different parts", parts.get(0), not(parts.get(1)));
    }

    private List<String> readAllParts() throws IOException, InterruptedException {
        List<String> parts = new ArrayList<String>();
        while (this.recordReader.nextKeyValue()) {
            parts.add(this.recordReader.getCurrentValue().toString());
        }
        return parts;
    }

    @Test
    public void readPartialFileFromStart() throws Exception {
        setupRecordReaderWith(0, getFullFileLength() / 2, TITLE_TAG);
        List<String> parts = readAllParts();

        assertThat("Expect not all parts", parts.size() < 25, is(true));
    }

    @Test
    public void readPartialFileFromMiddle() throws Exception {
        setupRecordReaderWith(getFullFileLength() / 2, getFullFileLength(), TITLE_TAG);
        List<String> parts = readAllParts();

        assertThat("Must find parts", parts.size() > 10, is(true));
        assertThat("Expect not all parts", parts.size() < 25, is(true));
    }
}
