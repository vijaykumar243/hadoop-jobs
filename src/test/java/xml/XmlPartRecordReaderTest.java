package xml;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordReader;
import wikipedia.WikipediaXmlRecorderReaderTests;

public class XmlPartRecordReaderTest extends WikipediaXmlRecorderReaderTests {

    @Override
    public RecordReader<Text, Text> createRecordReader() {
        return new XmlPartRecorderReader(new XmlPartReaderFactoryImpl());
    }
}
