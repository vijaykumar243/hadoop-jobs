package xml;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.internal.matchers.TypeSafeMatcher;

import java.io.*;
import java.util.Collection;
import java.util.List;

import static org.apache.hadoop.thirdparty.guava.common.collect.Lists.newArrayList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

abstract public class XmlPartReaderTestBase {

    public static final String XML_FILE = "src/main/resources/nlwiki-latest-pages-articles-short.xml";

    private final static String testXml =
            "<persons>" +
            "  <person>" +
            "    <name>Piet</name>" +
            "    <age>12</age>" +
            "  </person>" +
            "  <person>" +
            "    <name>Klaas</name>" +
            "    <age>44</age>" +
            "  </person>" +
            "</persons>";

    abstract XmlPartReader createReaderWith(String tagName, InputStream inputStream, long startPosition) throws IOException;

    private XmlPartReader createReaderWith(String tagName, InputStream inputStream) throws IOException {
        return createReaderWith(tagName, inputStream, 0L);
    }

    @Test
    public void readingFromPositionShouldReturnSomeResults() throws Exception {

        File file = new File(XML_FILE);
        long fileSize = file.length();
        FileInputStream inputStream = new FileInputStream(file);
        XmlPartReader reader = createReaderWith("title", inputStream, fileSize / 2);
        List<String> parts = readAllParts(reader);
        System.out.println("Found parts: "+parts.size());
        assertThat("Found some parts", parts, moreThen(0));
        assertThat("Less then all parts", parts, lessThen(37));
    }

    private Matcher<Collection> moreThen(final int size) {
        return new TypeSafeMatcher<Collection>() {
            @Override
            public boolean matchesSafely(Collection collection) {
                return collection.size() > size;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Collection size more then ").appendValue(size);
             }
        };
    }

    private Matcher<Collection> lessThen(final int size) {
        return new TypeSafeMatcher<Collection>() {
            @Override
            public boolean matchesSafely(Collection collection) {
                return collection.size() < size;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Collection size less then ").appendValue(size);
            }
        };
    }

    @Test
    public void closeReaderShouldNotThrowException() throws Exception {

        FileInputStream inputStream = new FileInputStream(XML_FILE);
        XmlPartReader reader = createReaderWith("title", inputStream);
        String part1 = reader.getNextXmlPart();
        reader.close();
    }

    @Test(expected = IOException.class)
    public void getPartShouldThrowIOExceptionWhenReaderIsClosedBeforeFirstRead() throws Exception {

        FileInputStream inputStream = new FileInputStream(XML_FILE);
        XmlPartReader reader = createReaderWith("title", inputStream);
        reader.close();
        String part1 = reader.getNextXmlPart();
    }

    @Test(expected = IOException.class)
    public void getPartShouldThrowIOExceptionWhenReaderIsClosedAfterRead() throws Exception {

        FileInputStream inputStream = new FileInputStream(XML_FILE);
        XmlPartReader reader = createReaderWith("title", inputStream);
        String part1 = reader.getNextXmlPart();
        reader.close();
        String part2 = reader.getNextXmlPart();
    }


    @Test
    public void hasNextShouldReturnFalseWhenTagNotInXml() throws Exception {

        ByteArrayInputStream xml = new ByteArrayInputStream(testXml.getBytes());
        XmlPartReader reader = createReaderWith("unknown", xml);
        assertThat(reader.hasNext(), is(false));
    }

    @Test
    public void nextTagShouldReturnNoTagsWhenTagNotInXml() throws Exception {

        ByteArrayInputStream xml = new ByteArrayInputStream(testXml.getBytes());
        XmlPartReader reader = createReaderWith("unknown", xml);
        List<String> parts = readAllParts(reader);
        assertThat(parts.size(), is(0));
    }

    @Test
    public void readerShouldReturnAllPartsFromString() throws Exception {

        ByteArrayInputStream xml = new ByteArrayInputStream(testXml.getBytes());
        XmlPartReader reader = createReaderWith("name", xml);
        List<String> parts = readAllParts(reader);

        assertThat(parts.size(), is(2));
        assertThat(parts.get(0), is("<name>Piet</name>"));
        assertThat(parts.get(1), is("<name>Klaas</name>"));
    }


    @Test
    public void hasNextReturnsTrue() throws Exception {

        FileInputStream inputStream = new FileInputStream(XML_FILE);
        XmlPartReader reader = createReaderWith("title", inputStream);
        assertThat(reader.hasNext(), is(true));
    }

    @Test
    public void nextTagReturnsTag() throws Exception {

        FileInputStream inputStream = new FileInputStream(XML_FILE);
        XmlPartReader reader = createReaderWith("title", inputStream);
        assertThat(reader.getNextXmlPart(), is("<title>Albert Speer</title>"));
    }

    @Test
    public void readAllTitleTags()  throws Exception {

        FileInputStream inputStream = new FileInputStream(XML_FILE);
        XmlPartReader reader = createReaderWith("title", inputStream);
        List<String> parts = readAllParts(reader);

        assertThat(parts.size(), is(37));
        assertThat(parts.get(0), not(parts.get(1)));
    }

    @Test
    public void readAllPageTags()  throws Exception {

        FileInputStream inputStream = new FileInputStream(XML_FILE);
        XmlPartReader reader = createReaderWith("page", inputStream);
        List<String> parts = readAllParts(reader);

        assertThat(parts.size(), is(37));
        assertThat(parts.get(0), not(parts.get(1)));
    }

    private List<String> readAllParts(XmlPartReader reader) throws IOException {
        List<String> parts = newArrayList();
        for(String part : reader) {
            parts.add(part);
        }
        return parts;
    }
}
