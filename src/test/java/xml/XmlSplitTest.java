package xml;

import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Iterator;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class XmlSplitTest {

    public static final String XML_FILE = "src/main/resources/nlwiki-latest-pages-articles-short.xml";

    @Test
    public void divideShouldReturnFloat() {

        float result = (7 - 2) / ((float)10 - 2);
        assertThat(result, is(0.625f));
    }

    @Test
    @Ignore // disabled because takes too long
    public void validateXml() throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        factory.setValidating(true);
        factory.setAttribute("http://java.sun.com/xml/jaxp/properties/schemaLanguage", "http://www.w3.org/2001/XMLSchema");
        DocumentBuilder builder = factory.newDocumentBuilder();
        builder.setErrorHandler(simpleErrorHandler());
        Document document = builder.parse(new InputSource(XML_FILE));
    }

    private ErrorHandler simpleErrorHandler() {
        return new ErrorHandler() {
            @Override
            public void warning(SAXParseException e) throws SAXException {
                System.out.println("WARN: "+e.getMessage());
            }

            @Override
            public void error(SAXParseException e) throws SAXException {
                System.out.println("ERR: "+e.getMessage());
            }

            @Override
            public void fatalError(SAXParseException e) throws SAXException {
                System.out.println("FATAL: "+e.getMessage());
            }
        };
    }

    @Test
    public void splitXml() throws Exception {

        String xmlFile = "src/test/resources/some.xml";
        XPath xpath = XPathFactory.newInstance().newXPath();
        xpath.setNamespaceContext(nameSpaceContextForURI("http://www.mediawiki.org/xml/export-0.6/"));
        String expr = "//page";
        NodeList list = (NodeList) xpath.evaluate("/:mediawiki/:page", new InputSource(XML_FILE), XPathConstants.NODESET);
        System.out.println(list.getLength());

        XPathExpression titleExpr = xpath.compile(".:title");
        for(Node node : new NodeListIterable(list)) {
            String title = (String) titleExpr.evaluate(node, XPathConstants.STRING);
            System.out.println("page title: "+title);
            System.out.println("page: "+ nodeToString(node));
        }

        Node node = list.item(0);
        String text = (String) xpath.evaluate(".//:text", node, XPathConstants.STRING);
//        System.out.println("text: "+text);
    }

    private String nodeToString(Node node) throws TransformerException {
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        StringWriter stringWriter = new StringWriter();
        transformer.transform(new DOMSource(node), new StreamResult(stringWriter));
        return stringWriter.toString();
    }

    private NamespaceContext nameSpaceContextForURI(final String uri) {
        return new NamespaceContext() {
            @Override
            public String getNamespaceURI(String s) {
                return uri;
            }

            @Override
            public String getPrefix(String s) {
                return null;
            }

            @Override
            public Iterator getPrefixes(String s) {
                return null;
            }
        };
    }

    private class NodeListIterable implements Iterable<Node> {

        private NodeList nodeList;
        private int currentNodeIndex = 0;

        public NodeListIterable(final NodeList nodeList) {
            super();
            this.nodeList = nodeList;
        }

        @Override
        public Iterator<Node> iterator() {
            return new Iterator<Node>() {

                @Override
                public boolean hasNext() {
                    return currentNodeIndex < nodeList.getLength();
                }

                @Override
                public Node next() {
                    return nodeList.item(currentNodeIndex++);
                }

                @Override
                public void remove() {
                    throw new IllegalArgumentException("Not implemented");
                }
            };
        }
    }

    private InputSource getResource() {
        InputStream resource = getClass().getClassLoader().getResourceAsStream("nlwiki-latest-pages-articles-short.xml");
        return new InputSource(resource);
    }
}
