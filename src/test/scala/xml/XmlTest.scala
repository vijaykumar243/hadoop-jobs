package xml

import io.{BufferedSource, Source}


object Test extends App {

  val xml = "src/main/resources/nlwiki-latest-pages-articles-short.xml"
  val source: BufferedSource = Source.fromFile(xml)

}