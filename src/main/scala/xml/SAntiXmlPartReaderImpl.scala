package xml

import java.io.InputStream
import scala.xml.pull.XMLEventReader
import io.Source

class SAntiXmlPartReaderImpl(tagName: String, inputStream: InputStream, startPosition: Long) extends XmlPartReader {

  val reader: XMLEventReader = new XMLEventReader(Source.fromInputStream(inputStream))

  def hasNext = false

  def getNextXmlPart = ""

  def iterator() = null

  def close() {}
}
