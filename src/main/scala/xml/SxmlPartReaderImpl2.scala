package xml

import java.io._


object TagPosition extends Enumeration {
  val BEGIN, END = Value
}

class SxmlPartReaderImpl2(tagName: String, inputStream: InputStream, startPosition: java.lang.Long) extends XmlPartReader with Closeable {

  val startTag = "<"+tagName+">"
  val tagChars = tagName.toCharArray()
  val skipped = inputStream.skip(startPosition)
  println("skipped: "+skipped)
  val inputReader = new BufferedInputStream(inputStream)

  var writer : StringWriter = null
  var startTagFound = false
  var currentChar : Char = 0

  def close() = inputStream.close();

  def iterator() = new XmlPartIterator(this)

  def hasNext = startTagFound || positionBeforeTag

  def getNextXmlPart = {
    if (hasNext) {
      readUntilEndTag
    } else null
  }

  private def readUntilEndTag = {
    writer = new StringWriter()
    writer.write(startTag)
    readUntilTag(TagPosition.END)
    val part = writer.toString
    startTagFound = false
    writer = null;
    part
  }

  private def readUntilTag(position: TagPosition.Value) = {
    var nextTagFound = false
    try {
      while(!nextTagFound) {
        readChar
        if (currentChar == '<') {
          readChar
          if ((position == TagPosition.END && currentChar == '/' && readChar) || (position == TagPosition.BEGIN && currentChar != '/')) {
            nextTagFound = isTagRead
          }
        }
      }
    } catch {
      case e: EOFException =>
    }
    nextTagFound
  }

  private def isTagRead = {
    var index = 0;
    while(currentChar == tagChars(index) && index < tagChars.length-1) {
      readChar
      index = index + 1
    }
    if (index == tagChars.length-1) {
      readChar
      currentChar == '>'
    }
    else false
  }

  private def positionBeforeTag = {
    startTagFound = readUntilTag(TagPosition.BEGIN)
    startTagFound
  }

  private def readChar = {
    val b = inputReader.read
    if(b == -1) throw new EOFException
    if (writer != null) writer.write(b)
    currentChar = b.toChar
    true
  }
}