package ch5

import org.apache.hadoop.conf.{Configured, Configuration}
import org.apache.hadoop.util.{Tool, ToolRunner}


class ScalaConfigurationPrinter extends Configured with Tool {

  def run(p1: Array[String]): Int = {
    import scala.collection.JavaConversions._
    val conf: Iterable[java.util.Map.Entry[String, String]] = getConf

    conf.foreach { entry => printf("%s=%s\n", entry.getKey, entry.getValue) }
    0
  }
}

object ScalaConfigurationPrinter extends App {

  Configuration.addDefaultResource("hdfs-default.xml");
  Configuration.addDefaultResource("hdfs-site.xml");
  Configuration.addDefaultResource("mapred-default.xml");
  Configuration.addDefaultResource("mapred-site.xml");
  Configuration.addDefaultResource("core-default.xml");
  Configuration.addDefaultResource("core-site.xml");

  ToolRunner.run(new ScalaConfigurationPrinter(), args)
}