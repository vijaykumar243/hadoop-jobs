#!/bin/sh
  exec scala "$0" "$@"
!#

val validQualities = """[01459]""".r

for(ln <- io.Source.stdin.getLines) {
  val year = ln.substring(15, 19)
  val temp = ln.substring(87, 92)
  val quality = ""+ln(92)

  validQualities findFirstIn quality match {
    case Some(_) => println("%s\t%s" format (year, temp))
    case None =>
  }
}
