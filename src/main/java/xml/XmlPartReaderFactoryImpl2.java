package xml;

import java.io.IOException;
import java.io.InputStream;

public class XmlPartReaderFactoryImpl2 implements XmlPartReaderFactory {

    @Override
    public XmlPartReader readerFor(String tag, InputStream inputStream, long startPosition) throws IOException {
        return new XmlPartReaderImpl2(tag, inputStream, startPosition);
    }
}
