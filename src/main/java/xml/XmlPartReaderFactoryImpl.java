package xml;

import java.io.IOException;
import java.io.InputStream;

public class XmlPartReaderFactoryImpl implements XmlPartReaderFactory {

    @Override
    public XmlPartReader readerFor(String tag, InputStream inputStream, long startPosition) throws IOException {
        return XmlPartReaderImpl.readerFor(tag, inputStream, startPosition);
    }
}
