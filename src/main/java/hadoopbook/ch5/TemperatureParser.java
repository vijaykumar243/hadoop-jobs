package hadoopbook.ch5;

public class TemperatureParser {

    private String measurement;

    public TemperatureParser(String measurement) {
        this.measurement = measurement;
    }

    public String getYear() {
        return measurement.toString().substring(15, 19);
    }

    public int getTemperature() {
        String temp = measurement.toString().substring(87, 92);
        return temp.startsWith("+") ? Integer.parseInt(temp.substring(1)) : Integer.parseInt(temp);
    }

    public int getTemperatureQuality() {
        return 0;
    }

    public boolean isTemperatureMissing() {
        return getTemperature() == 9999;
    }
}
