package hadoopbook.ch5;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import util.Collections3;
import util.Function2;

import java.io.IOException;

public class MaxTemperatureReducerV2 extends Reducer<Text, IntWritable, Text, IntWritable> {

    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {

        int max = max(values);
//        System.out.println("Max is : "+max);
        context.write(key, new IntWritable(max));
    }

    private int max(Iterable<IntWritable> values) {
        return Collections3.foldLeft(values, 0, MAX);
    }

    public static final Function2<IntWritable,Integer> MAX = new Function2<IntWritable, Integer>() {
        @Override
        public Integer apply(Integer max, IntWritable value) {
            return Math.max(max, value.get());
        }
    };
}
