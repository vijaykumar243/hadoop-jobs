package hadoopbook.ch5;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import util.Collections3;
import util.Function2;

public class FunctionalMaxTemperatureMapperReducer extends FunctionalMapperReducer<LongWritable, Text, Text, IntWritable, Text, IntWritable> {

    public static final Function2<IntWritable,Integer> MAX = new Function2<IntWritable, Integer>() {
        @Override
        public Integer apply(Integer max, IntWritable value) {
            return Math.max(max, value.get());
        }
    };

    @Override
    MRFunction<LongWritable, Text, Text, IntWritable> mapper() {
        return new MRFunction<LongWritable, Text, Text, IntWritable>() {
            @Override
            public Result<Text, IntWritable> apply(LongWritable key, Text value) {
                TemperatureParser parser = new TemperatureParser(value.toString());
                if(!parser.isTemperatureMissing()) {
                    return new Result<Text, IntWritable>(new Text(parser.getYear()), new IntWritable(parser.getTemperature()));
                } else {
                    return RESULT_SKIP;
                }
            }
        };
    }

    @Override
    MRFunction<Text, Iterable<IntWritable>, Text, IntWritable> reducer() {
        return new MRFunction<Text, Iterable<IntWritable>, Text, IntWritable>() {
            @Override
            public Result<Text, IntWritable> apply(Text key, Iterable<IntWritable> values) {
                Integer max = Collections3.foldLeft(values, Integer.MIN_VALUE, MAX);
                return new Result<Text, IntWritable>(key, new IntWritable(max));
            }
        };
    }
}
