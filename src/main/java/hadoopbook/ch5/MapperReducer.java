package hadoopbook.ch5;

import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

abstract public class MapperReducer <K1, V1, K2, V2, K3, V3> {

    abstract public void map1(K1 key, V1 value, Mapper.Context context) throws IOException, InterruptedException;

    abstract public void reduce1(K2 key, Iterable<V2> values, Reducer.Context context) throws IOException, InterruptedException;

    public Mapper<K1, V1, K2, V2> createMapper() {
        return new MRMapper();
    }

    public Reducer<K2, V2, K3, V3> createReducer() {
        return new MRReducer();
    }

    public class MRMapper extends Mapper<K1, V1, K2, V2> {
        @Override
        protected void map(K1 key, V1 value, Context context) throws IOException, InterruptedException {
            map1(key, value, context);
        }
    }

    public class MRReducer extends Reducer<K2, V2, K3, V3> {
        @Override
        protected void reduce(K2 key, Iterable<V2> values, Context context) throws IOException, InterruptedException {
            reduce1(key, values, context);
        }
    }
}
