package wikipedia;

import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import xml.XmlPartReaderImpl;

import java.io.IOException;

public class WikipediaXmlRecorderReader extends RecordReader<Text, Text> {

    public static final String XML_READER_TAG = "xml.reader.tag";

    private FileSplit split;
    private FSDataInputStream fileInputStream;
    private long start;
    private long end;
    private XmlPartReaderImpl xmlPartReader;
    private long position;
    private Text valueText;
    private Text keyText;

    @Override
    public void initialize(InputSplit inputSplit, TaskAttemptContext context) throws IOException, InterruptedException {
        this.split = (FileSplit) inputSplit;

        Path filePath = split.getPath();
        FileSystem fileSystem = filePath.getFileSystem(context.getConfiguration());
        fileInputStream = fileSystem.open(filePath);

        String tagName = context.getConfiguration().get(XML_READER_TAG, "page");

        start = split.getStart();
        end = start + split.getLength();
        position = start;

        xmlPartReader = XmlPartReaderImpl.readerFor(tagName, fileInputStream, start);

        keyText = new Text(tagName);
        valueText = new Text();
    }

    @Override
    public boolean nextKeyValue() throws IOException, InterruptedException {
        if(position < end && xmlPartReader.hasNext()) {

            valueText.set(xmlPartReader.getNextXmlPart());
            position = fileInputStream.getPos() - 1; // minus 1 otherwise misses last part

            return true;
        }
        return false;
    }

    @Override
    public Text getCurrentKey() throws IOException, InterruptedException {
        return keyText;
    }

    @Override
    public Text getCurrentValue() throws IOException, InterruptedException {
        return valueText;
    }

    @Override
    public float getProgress() throws IOException, InterruptedException {
        return (position - start) / ((float)end - start);
    }

    @Override
    public void close() throws IOException {
        xmlPartReader.close();
    }
}
